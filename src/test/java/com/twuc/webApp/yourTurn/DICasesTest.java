package com.twuc.webApp.yourTurn;

import com.twuc.webApp.over.OutOfScanningScope;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class DICasesTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp(){
        context = new AnnotationConfigApplicationContext("com.twuc.webapp.yourTurn");
    }

    // 2.1 -1
    @Test
    void should_test_object_without_dependency() {
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
    }

    // 2.1 -2  scope-prototype
    @Test
    void should_the_same_object() {
        WithoutDependency bean1 = context.getBean(WithoutDependency.class);
        WithoutDependency bean2 = context.getBean(WithoutDependency.class);
        Dependent dependent = context.getBean(Dependent.class);
        assertNotEquals(bean1,bean2);
        assertNotNull(dependent);
    }

    // 2.1 -3
    @Test
    void should_scan_bean_over_range() {
        assertThrows(RuntimeException.class, () -> {
                context.getBean(OutOfScanningScope.class);
        });
    }

    // 2.1 -4
    @Test
    void should_verify_interface() {
        InterfaceImpl interfaceOne = (InterfaceImpl) context.getBean(InterfaceOne.class);
        assertNotNull(interfaceOne);
    }

    // 2.1 -5
    @Test
    void should_verify_simple_constructor() {
        SimpleObject simpleObject = (SimpleObject)context.getBean(SimpleInterface.class);
        SimpleDependent simpleDependent = simpleObject.getSimpleDependent();
        assertEquals("O_o", simpleDependent.getName());
    }

    // 2.2 -1
    @Test
    void should_test_constructor() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean);
    }

    // 2.2 -2
    @Test
    void should_test_order_of_calling_init_and_constructor() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        assertNotNull(bean);
        assertEquals(Arrays.asList("constructor", "init method"), bean.getMyLogger().getLogger());
    }
    
    // 2.2 -3
    @Test
    void should_return_array_of_all_implement_class_instance() {
        String[] names = context.getBeanNamesForType(InterfaceWithMultipleImpls.class);
        ImplementationA beanA = (ImplementationA) context.getBean(names[0]);
        assertNotNull(beanA);
        ImplementationB beanB = (ImplementationB) context.getBean(names[1]);
        assertNotNull(beanB);
        ImplementationC beanC = (ImplementationC) context.getBean(names[2]);
        assertNotNull(beanC);
    }
}
