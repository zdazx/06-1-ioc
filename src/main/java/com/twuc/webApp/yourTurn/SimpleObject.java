package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SimpleObject implements SimpleInterface {
    private SimpleDependent simpleDependent;

    public SimpleObject(SimpleDependent simpleDependent) {
        this.simpleDependent = simpleDependent;
    }

    @Bean
    public SimpleDependent getSimpleDependent(){
        simpleDependent.setName("O_o");
        return simpleDependent;
    }
}
