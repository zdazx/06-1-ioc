package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class SimpleDependent {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    // 发现一个有意思的情况：
    // 若该方法返回return "O_o";
    // 则logger打印结果为{"O_o", "constructor", "init method"}
    @Bean
    public String getName() {
        return this.name;
    }

}
