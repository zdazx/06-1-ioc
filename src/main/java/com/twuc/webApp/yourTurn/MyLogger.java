package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MyLogger {
    private List<String> logger;

    public MyLogger(List<String> logger) {
        this.logger = logger;
    }

    public void write(String info){
        logger.add(info);
    }

    public List<String> getLogger(){
        return logger;
    }
}
