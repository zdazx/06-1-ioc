package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WithAutowiredMethod {
    private Dependent dependent;
    private MyLogger myLogger;

    public WithAutowiredMethod(Dependent dependent, MyLogger myLogger) {
        this.dependent = dependent;
        this.myLogger = myLogger;
        this.myLogger.write("constructor");
    }

    @Autowired
    public void initialize(AnotherDependent AnotherDependent){
        this.myLogger.write("init method");
    }

   public MyLogger getMyLogger() {
        return myLogger;
    }
}
